let express=require('express');
let router=express.Router();
let mongoose =require('mongoose');
const Blog=require("../models/blogs");
let mongodbUri="mongodb+srv://Archie:sunyifanqwert@cluster0-mxku3.mongodb.net/blogdb?retryWrites=true&w=majority";
mongoose.connect(mongodbUri);
let db = mongoose.connection;

db.on('error', function (err) {
    console.log('Unable to Connect to [ ' + db.name + ' ]', err);
});

db.once('open', function () {
    console.log('Successfully Connected to [ ' + db.name + ' ]');
});

//method
//getAll
router.findAll = (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    Blog.find(function(err, bls) {
        if (err) {res.send(err);}
        res.send(JSON.stringify(bls,null,5));
    });
};

//addBlog
router.addBlog = (req,res) => {
    let blog=new Blog();
    blog.userId=req.params.userId;
    blog.title=req.body.title;
    blog.content=req.body.content;
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth();
    let date = now.getDate();
    let hour = now.getHours();
    let minu = now.getMinutes();
    let sec = now.getSeconds();
    let time=year+"-"+month+"-"+date+"-"+hour+"-"+minu+"-"+sec;
    blog.time=time;
    blog.save(function (err) {
        if(err)
            res.send(err);
        else{
            res.json({ message: "Add successfully" });
        }
    })
}
//deleteBlog
router.deleteBlog=(req,res)=>{
    Blog.findByIdAndDelete(req.params.id,function(err){
        if(err){
            res.json({ message: "Not Exist" });
        }else {
            res.json({ message: "Delete successfully" });
        }
    })
}
//alterBlog
router.alterBlog=(req,res)=>{
    let newTitle=req.body.title;
    let newContent=req.body.content;
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth();
    let date = now.getDate();
    let hour = now.getHours();
    let minu = now.getMinutes();
    let sec = now.getSeconds();
    let newTime=year+"-"+month+"-"+date+"-"+hour+"-"+minu+"-"+sec;
    Blog.findById(req.params.id,function(err,bl){
        if(err){
            res.json({ message: "Not Exist" });
        }else {
            bl.title=newTitle;
            bl.content=newContent;
            bl.time=newTime;
            bl.save(function(err){
                if(err){
                    res.json({ message: "Not Exist" });
                }else{
                    res.json({ message: "Alter successfully" });
                }
            })
        }
    })
}
//findBlogByUser
router.findBlogByUser=(req,res)=>{
    res.setHeader('Content-Type', 'application/json');
    Blog.find({"userId":req.params.userId},function (err,bls) {
        if(err){
            res.json({ message: "Not Exist" });
        }else{
            res.send(JSON.stringify(bls,null,5));
        }

    })
}
//searchBlog
router.searchBlog=(req,res)=>{
    res.setHeader('Content-Type', 'application/json');
    Blog.find({"title":{$regex:req.params.keyword,$options:'$i'}},function (err,bls) {
        if(err){
            res.send(err);
        }else{
            res.send(JSON.stringify(bls,null,5));
        }
    })
}


module.exports = router;