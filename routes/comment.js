let express=require('express');
let router=express.Router();
let mongoose =require('mongoose');
const Comment=require("../models/comments");
let mongodbUri="mongodb+srv://Archie:sunyifanqwert@cluster0-mxku3.mongodb.net/blogdb?retryWrites=true&w=majority";
mongoose.connect(mongodbUri);
let db = mongoose.connection;

db.on('error', function (err) {
    console.log('Unable to Connect to [ ' + db.name + ' ]', err);
});

db.once('open', function () {
    console.log('Successfully Connected to [ ' + db.name + ' ]');
});

//method
//addBlog
router.addComment = (req,res) => {
    let comment=new Comment();
    comment.userId=req.params.userId;
    comment.blogId=req.params.blogId;
    comment.content=req.body.content;
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth();
    let date = now.getDate();
    let hour = now.getHours();
    let minu = now.getMinutes();
    let sec = now.getSeconds();
    let time=year+"-"+month+"-"+date+"-"+hour+"-"+minu+"-"+sec;
    comment.time=time;
    comment.save(function (err) {
        if(err)
            res.send(err);
        else{
            res.json({ message: "Add successfully" });
        }
    })
}
//findCommentByBlogId
router.findCommentByBlog=(req,res)=>{
    res.setHeader('Content-Type', 'application/json');
    Comment.find({"blogId":req.params.blogId},function (err,cms) {
        if(err){
            res.json({ message: "Not Exist" });
        }else{
            res.send(JSON.stringify(cms,null,5));
        }

    })
}

//findCommentByUSerId
router.findCommentByUser=(req,res)=>{
    res.setHeader('Content-Type', 'application/json');
    Comment.find({"userId":req.params.userId},function (err,cms) {
        if(err){
            res.json({ message: "Not Exist" });
        }else{
            res.send(JSON.stringify(cms,null,5));
        }

    })
}

//findCommentById
router.findComment=(req,res)=>{
    res.setHeader('Content-Type', 'application/json');
    Comment.find({"_id":req.params.id},function (err,cms) {
        if(err){
            res.json({ message: "Not Exist" });
        }else{
            res.send(JSON.stringify(cms,null,5));
        }

    })
}

//alterComment
router.alterComment=(req,res)=>{
    let newContent=req.body.content;
    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth();
    let date = now.getDate();
    let hour = now.getHours();
    let minu = now.getMinutes();
    let sec = now.getSeconds();
    let newTime=year+"-"+month+"-"+date+"-"+hour+"-"+minu+"-"+sec;
    Comment.findById(req.params.id,function(err,cm){
        if(err){
            res.json({ message: "Not Exist" });
        }else {
            cm.content=newContent;
            cm.time=newTime;
            cm.save(function(err){
                if(err){
                    res.json({ message: "Not Exist" });
                }else{
                    res.json({ message: "Alter successfully" });
                }
            })
        }
    })
}

//deleteComment
router.deleteComment=(req,res)=>{
    Comment.findByIdAndDelete(req.params.id,function(err){
        if(err){
            res.json({ message: "Not Exist" });
        }else {
            res.json({ message: "Delete successfully" });
        }
    })
}

module.exports = router;