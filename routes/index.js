var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendfile('./views/register.html');
});
router.get('/login.html', function(req, res, next) {
  res.sendfile('./views/login.html');
});


module.exports = router;
