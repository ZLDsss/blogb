let mongoose = require('mongoose');

let UserSchema = new mongoose.Schema({
        account: String,
        password: String,
        name: String
    },
    { collection: 'users' });

module.exports = mongoose.model('User', UserSchema);