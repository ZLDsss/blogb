let mongoose = require('mongoose');

let BlogSchema = new mongoose.Schema({
        userId: String,
        title: String,
        content: String,
        time: String,
    },
    { collection: 'blogs' });

module.exports = mongoose.model('Blog', BlogSchema);